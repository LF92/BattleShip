package com.company;

/**
 * Created by lukasz on 11.04.17.
 */
public class IllegalMoveException extends Exception {

    public IllegalMoveException(String message) {
        super(message);
    }

}
