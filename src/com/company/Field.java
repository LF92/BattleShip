package com.company;

/**
 * Created by lukasz on 29.03.17.
 */
public class Field {

    private State state;
    private int x;
    private int y;
    private Ship ship;


    public Field(int x, int y, State state) {
        this.x = x;
        this.y = y;
        this.state = state;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }

    public char stateToChar() {
        char value;

        switch (this.state) {
            case EMPTY:
                value = ' ';
                break;
            case HIT:
                value = 'O';
                break;
            default:
                value = '.';
        }
        return value;
    }

    public void setState(State state) {
        this.state = state;
    }
}
