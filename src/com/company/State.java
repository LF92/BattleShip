package com.company;

/**
 * Created by lukasz on 29.03.17.
 */

enum State {
    EMPTY, SHIP, HIT, MISS, SUNK
}
