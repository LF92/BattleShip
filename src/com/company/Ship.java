package com.company;

/**
 * Created by lukasz on 10.04.17.
 */
public interface Ship {

    int getDecksCount();

    void hit();

    boolean isSunk();

    void setOnField(Field field, int deckNo);

}
